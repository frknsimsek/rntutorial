import { useState } from "react";
import { StyleSheet, ImageBackground, SafeAreaView } from "react-native";

import StartGameScreen from "./screens/StartGameScreen";
import GameScreen from "./screens/GameScreen";
import GameOverScreen from "./screens/GameOverScreen";

export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true);

  function pickedNumberHandler(pickedNumber) {
    setUserNumber(pickedNumber);
    setGameIsOver(false);
  }

  let screen = <StartGameScreen onPickNumber={pickedNumberHandler} />;

  if (userNumber) {
    screen = (
      <GameScreen userNumber={userNumber} onGameOver={gameOverHandler} />
    );
  }

  if (gameIsOver && userNumber) {
    screen = <GameOverScreen onStartNewGame={startNewGameHandler} />;
  }

  function gameOverHandler() {
    setGameIsOver(true);
  }
  function startNewGameHandler() {
    setUserNumber(null);
    setGameIsOver(true);
  }
  return (
    <SafeAreaView style={styles.screen}>
      <ImageBackground
        style={styles.screen}
        imageStyle={styles.backgroundContainer}
        source={require("./assets/image/photo1.png")}
        resizeMode="cover"
      >
        {screen}
      </ImageBackground>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  backgroundImage: {
    opacity: 0.15,
  },
});
