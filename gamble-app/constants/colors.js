const Color = {
  primary500: "black",
  primary600: "#9e8790",
  primary650: "#ddb52f",
  primary700: "white",
};
export default Color;
