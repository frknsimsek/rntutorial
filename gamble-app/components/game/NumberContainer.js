import { Text, View, StyleSheet } from "react-native";
import Color from "../../constants/colors";

function NumberContainer({ children }) {
  return (
    <View style={styles.container}>
      <Text style={styles.numberText}>{children}</Text>
    </View>
  );
}

export default NumberContainer;

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: Color.primary700,
    justifyContent: "center",
    alignItems: "center",
    margin: 24,
    padding: 24,
    borderRadius: 30,
  },
  numberText: {
    fontSize: 30,
    fontWeight: "bold",
    color: Color.primary700,
  },
});
