import { Text, StyleSheet, View } from "react-native";
import PrimaryButton from "../components/PrimaryButton";

function GameOverScreen({ onStartNewGame }) {
  return (
    <View style={styles.rootScreenContainer}>
      <Text style={styles.textContainer}>Game is Over!!</Text>

      <PrimaryButton onPress={onStartNewGame} title="Start New Game">
        Start New Game
      </PrimaryButton>
    </View>
  );
}
const styles = StyleSheet.create({
  rootScreenContainer: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  textContainer: {
    borderWidth: 3,
    borderColor: "white",
    color: "white",
    fontSize: 24,
    borderRadius: 24,
    padding: 20,
  },
});
export default GameOverScreen;
