import { StyleSheet, View, Text, Pressable } from "react-native";
//Return etmeyi unutma!!!!!
function ListItem(props) {
  return (
    <View styles={styles.listItem}>
      <Pressable
        android_ripple={{ color: "#210644" }}
        onPress={props.onDelete.bind(this, props.id)}
        style={({ pressed }) => pressed && styles.pressedItem}
      >
        <Text style={styles.listText}>{props.text}</Text>
      </Pressable>
    </View>
  );
}
export default ListItem;
const styles = StyleSheet.create({
  listItem: {
    margin: 8,
    borderRadius: 6,
    backgroundColor: "#5e0acc",
  },
  listText: {
    color: "white",
    padding: 5,
    marginBottom: 5,
    borderRadius: 5,
    backgroundColor: "#7fd2c9",
  },
  pressedItem: {
    opacity: 0.5,
  },
});
