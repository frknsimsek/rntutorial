import { useState } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Button,
  Modal,
  Image,
} from "react-native";

function ListInput(props) {
  const [enteredListText, setEnteredListText] = useState("");

  function listInputCheck(enteredText) {
    setEnteredListText(enteredText);
  }

  function buttonAddFunction() {
    props.onAddList(enteredListText);
    setEnteredListText("");
  }

  return (
    <Modal visible={props.visible} animationType="fade">
      <View style={styles.inputContainer}>
        <Image
          style={styles.imageKiz}
          source={require("../assets/image/kiz.png")}
        />
        <TextInput
          style={styles.textInput}
          placeholder="Buraya Yazın"
          onChangeText={listInputCheck}
          value={enteredListText}
        />
        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button
              color={"#833baa"}
              title="Tap here"
              onPress={buttonAddFunction}
            ></Button>
          </View>
          <View style={styles.button}>
            <Button
              color={"#833baa"}
              title="Cancel"
              onPress={props.onCancel}
            ></Button>
          </View>
        </View>
      </View>
    </Modal>
  );
}
export default ListInput;

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: "#ffd1dc",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 24,
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: "#cccccc",
  },
  textInput: {
    borderWidth: 1,
    borderColor: "#cccccc",
    width: "100%",
    padding: 8,
  },
  buttonContainer: {
    marginTop: 16,
    flexDirection: "row",
  },
  button: {
    width: 100,
    marginHorizontal: 8,
    color: "#833baa",
  },
  imageKiz: {
    width: 300,
    height: 368,
    margin: 20,
  },
});
