import { useState } from "react";
import { StyleSheet, Text, View, FlatList, Button } from "react-native";
import ListInput from "./components/ListInput";
import ListItem from "./components/ListItem";

export default function App() {
  const [listElement, setListElement] = useState([]);
  const [modalIsVisible, setModalIsVisible] = useState(false);

  function startListButton() {
    setModalIsVisible(true);
  }

  function endListButton() {
    setModalIsVisible(false);
  }
  function buttonAddFunction(enteredListText) {
    setListElement((currentListElement) => [
      ...currentListElement,
      {
        text: enteredListText,
        id: Math.random().toString(),
      },
    ]);
    endListButton();
  }

  function deleteFunction(id) {
    setListElement((currentListElement) => {
      return currentListElement.filter((goal) => goal.id !== id);
    });
  }

  return (
    <View style={styles.appContainer}>
      <Button
        color={"#833baa"}
        title="Add New Item For List"
        onPress={startListButton}
      />
      <ListInput
        onAddList={buttonAddFunction}
        visible={modalIsVisible}
        onCancel={endListButton}
      />

      <View style={styles.listContainer}>
        <FlatList
          data={listElement}
          renderItem={(itemData) => {
            return (
              <ListItem
                text={itemData.item.text}
                id={itemData.item.id}
                onDelete={deleteFunction}
              />
            );
          }}
          keyExtractor={(item, index) => {
            return item.id;
          }}
        ></FlatList>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 2,
  },

  listContainer: {
    margin: 5,
    flex: 3,
    backgroundColor: "#ffd1dc",
  },
});
